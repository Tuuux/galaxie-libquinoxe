# galaxie-libquinoxe
Once upon a time, this project was hosted on a 
ancient platform called GitHub. Then came the Buyer.
The Buyer bought GitHub, willing to rule over its community.
I was not to sell, so here is the new home of "https://github.com/Tuuux/galaxie-libquinoxe".

low tech lib it focus on important date or cool math thing

### From 2024 to 2017 equinox date

```
lun. 23 sept. 2024 21:57:41 UTC

Year:2024
March equinox:     Wed Mar 20 03:06:22 2024 in -188 days
Summer Solstice:   Thu Jun 20 20:51:03 2024 in -95 days
September equinox: Sun Sep 22 12:43:32 2024 in -1 day
Winter Solstice:   Sat Dec 21 09:20:20 2024 in 88 days

Year:2025
March equinox:     Thu Mar 20 09:01:14 2025 in 177 days
Summer Solstice:   Sat Jun 21 02:42:18 2025 in 270 days
September equinox: Mon Sep 22 18:19:17 2025 in 364 days
Winter Solstice:   Sun Dec 21 15:02:51 2025 in 454 days

Year:2026
March equinox:     Fri Mar 20 14:45:53 2026 in 543 days
Summer Solstice:   Sun Jun 21 08:24:31 2026 in 635 days
September equinox: Wed Sep 23 00:05:09 2026 in 729 days
Winter Solstice:   Mon Dec 21 20:49:59 2026 in 819 days

Year:2027
March equinox:     Sat Mar 20 20:24:32 2027 in 908 days
Summer Solstice:   Mon Jun 21 14:10:50 2027 in 1001 days
September equinox: Thu Sep 23 06:01:32 2027 in 1094 days
Winter Solstice:   Wed Dec 22 02:41:53 2027 in 1184 days
```

###Circle div by a number contain in power of two list

```
0: 360 / 0 = 360 -> 9
1: 360 / 2 = 180 -> 9
2: 360 / 4 = 90 -> 9
3: 360 / 8 = 45 -> 9
4: 360 / 16 = 22.5 -> 9
5: 360 / 32 = 11.25 -> 9
6: 360 / 64 = 5.625 -> 9
7: 360 / 128 = 2.8125 -> 9
8: 360 / 256 = 1.40625 -> 9
9: 360 / 512 = 0.703125 -> 9
10: 360 / 1024 = 0.3515625 -> 9
11: 360 / 2048 = 0.17578125 -> 9
12: 360 / 4096 = 0.087890625 -> 9
13: 360 / 8192 = 0.0439453125 -> 9
14: 360 / 16384 = 0.02197265625 -> 9
15: 360 / 32768 = 0.010986328125 -> 9
16: 360 / 65536 = 0.0054931640625 -> 9
```

