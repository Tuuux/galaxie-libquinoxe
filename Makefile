.PHONY: help
help: header


	@echo  ''
	@echo  'CleaningUp:'
	@echo  '  clean             - Remove every created directory and restart from scratch'
	@echo  ''
	@echo  'Documentation:'
	@echo  '  documentations    - Sphinx command it call venv, install docs/requirements.txt'
	@echo  '                      requirements, run sphinx-apidoc, and generate html '
	@echo  '                      documentation.'
	@echo  '                      Documentation is store on ./docs/source and the '
	@echo  '                      documentation build on ./docs/build'
	@echo  ''
	@echo  'VirtualEnvironment:'
	@echo  '  prepare           - Call venv-create and install or update packages:'
	@echo  '                      pip, wheel and setuptools'
	@echo  ''

.PHONY: install-python
install-python:
	@echo 	""
	@echo 	"******************************* INSTALL PYTHON *********************************"
	@apt-get update && apt install -y python3 python3-pip python3-venv

header:
	@echo "****************************** GALAXIE LIBQUINOXE ******************************"
	@echo ""
	@echo "License WTFPL V2"
	@echo "LOADER `uname -v`"
	@echo "EXEC MAKE"
	@echo "********************************************************************************"

##
## —————————————— ENVIRONEMENT —————————————————————————————————————————————————————
##
.PHONY: prepare
prepare: header ## Prepare virtual environment
	@pip3 install -U pip --no-cache-dir --quiet &&\
	echo "[  OK  ] INSTALL PIP3" || \
	echo "[FAILED] INSTALL PIP3"

	@pip3 install -U wheel --no-cache-dir --quiet &&\
	echo "[  OK  ] INSTALL WHEEL" || \
	echo "[FAILED] INSTALL WHEEL"

	@pip3 install -U setuptools --no-cache-dir --quiet &&\
	echo "[  OK  ] INSTALL SETUPTOOLS" || \
	echo "[FAILED] INSTALL SETUPTOOLS"

	@pip3 install -U -e .[tests] --no-cache-dir --quiet &&\
	echo "[  OK  ] INSTALL LIBQUINOXE AS DEV MODE" || \
	echo "[FAILED] INSTALL LIBQUINOXE AS DEV MODE"


.PHONY: tests
tests:
	@echo '********************************** RUN TESTS ***********************************'
	@ pytest

.PHONY: docs
docs:
	@echo '***************************** BUILD DOCUMENTATIONS *****************************'
	@pip3 install -r docs/requirements.txt --no-cache-dir --quiet &&\
    cd $(PWD)/docs && make html

clean:
	@echo '********************************* CLEANING UP **********************************'
	rm -rf ./.eggs
	rm -rf ./build
	rm -rf ./dist
	rm -rf ./*.egg-info
	rm -rf ./*.spec
	rm -rf ./venv
	rm -rf ./.direnv
	rm -rf ./pyenv
	rm -rf ./env
	rm -rf ./dep
	rm -rf ./docs/build
