import unittest
import libquinoxe


class TestCircleDivByTwo(unittest.TestCase):
    def test_addition_digit(self):
        circle = libquinoxe.Circle()
        self.assertEqual(9, circle.addition_digit(36))
        self.assertEqual(9, circle.addition_digit(3.6))

    # def test_compute(self):
    #     circle = libquinoxe.Circle()
    #     circle.compute()


if __name__ == "__main__":
    unittest.main()
