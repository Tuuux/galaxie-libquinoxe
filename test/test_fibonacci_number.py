import unittest
import libquinoxe


class TestFiboncciNumber(unittest.TestCase):
    def test_list_size(self):
        fibonacci_number = libquinoxe.FibonacciNumber()
        self.assertEqual(0, fibonacci_number.list_size)
        fibonacci_number.list_size = 42
        self.assertEqual(42, fibonacci_number.list_size)
        fibonacci_number.list_size = None
        self.assertEqual(0, fibonacci_number.list_size)

        self.assertRaises(TypeError, setattr, fibonacci_number, "list_size", "Hello.42")

    def test_list(self):
        fibonacci_number = libquinoxe.FibonacciNumber()
        self.assertEqual([0, 1], fibonacci_number.list)
        fibonacci_number.list = [0, 1, 42]
        self.assertEqual([0, 1, 42], fibonacci_number.list)
        fibonacci_number.list = None
        self.assertEqual([0, 1], fibonacci_number.list)

        self.assertRaises(TypeError, setattr, fibonacci_number, "list", "Hello.42")

    def test_compute(self):
        fibonacci_number = libquinoxe.FibonacciNumber()
        fibonacci_number.list_size = 10
        fibonacci_number.compute()
        self.assertEqual([0, 1, 1, 2, 3, 5, 8, 13, 21, 34], fibonacci_number.list)

    def test_compute_big(self):
        fibonacci_number = libquinoxe.FibonacciNumber()
        fibonacci_number.list_size = 4000
        fibonacci_number.compute()


if __name__ == "__main__":
    unittest.main()
