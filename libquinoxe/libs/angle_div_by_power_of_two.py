#! /usr/bin/env python


from decimal import *


class Circle(object):
    def __init__(self):
        self.__power_of_two = None
        self.__angle = None
        self.angle = 360

        self.power_of_two = None

    @property
    def power_of_two(self):
        return self.__power_of_two

    @power_of_two.setter
    def power_of_two(self, value=None):
        if value is None:
            value = [0, 2]
        if self.power_of_two != value:
            self.__power_of_two = value

    def increase_power_of_two(self):
        self.power_of_two.append(self.power_of_two[-1] * 2)

    def compute(self):
        value = 0
        iteration = 0
        while True:
            try:
                self.increase_power_of_two()
                if value == 0:
                    print(
                        "{4}: {0} / {1} = {2} -> {3}".format(
                            self.angle,
                            value,
                            self.angle,
                            self.addition_digit(self.angle),
                            iteration,
                        )
                    )
                    iteration += 1
                elif value in self.power_of_two:
                    estimated = Decimal(self.angle / value)
                    print(
                        "{4}: {0} / {1} = {2} -> {3}".format(
                            self.angle,
                            value,
                            estimated,
                            self.addition_digit(estimated),
                            iteration,
                        )
                    )
                    iteration += 1
                value += 1

            except KeyboardInterrupt:
                break

    @staticmethod
    def addition_digit(number):
        addition = 0
        second_level_addition = 0
        for number in str(number).replace(".", "", 1):
            try:
                addition += int(number)
            except ValueError:
                pass
        if len(str(addition)) == 2:
            second_level_number_string = str(addition).replace(".", "", 1)
            for number in second_level_number_string:
                second_level_addition += int(number)
            return second_level_addition
        return addition
