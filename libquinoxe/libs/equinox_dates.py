#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: "Tuux" <tuxa@rtnp.org> all rights reserved

# Inspire by: https://chrisramsay.co.uk/posts/2017/03/fun-with-the-sun-and-pyephem/

import sys
import ephem
import datetime
import locale

locale.setlocale(locale.LC_TIME, "")


class EquinoxDates(object):
    """
    That class we centralize all computing about Sun Equinox and Solstice
    """

    def __init__(self, **kwargs):
        """
        Sun Function

        .. py:attribute:: year - The year from when compute thing

        :param for_year: A year from when the computation will be done, if None the used year will be the actual Year.
        :type for_year: int or None
        """
        self.__year = None
        self.year = None

        self.year = kwargs.get('for_year', None)

    @property
    def year(self):
        """
        Store the year from where the computation will be done
        
        :getter: Set the year
        :setter: Set the year
        :rtype: int
        """
        return self.__year

    @year.setter
    def year(self, value):
        if value is None:
            value = int(datetime.datetime.now().strftime("%Y"))
        if not isinstance(value, int):
            value = int(datetime.datetime.now().strftime("%Y"))
        if self.year != value:
            self.__year = value

    def get_march_equinox(self, for_year=None):
        """
        Get the March equinox.

        There are two equinoxes every year – in March and September – when the Sun shines directly on the equator and
        the length of night and day are nearly equal.

        :param for_year: The year from when get the march equinox, if None it will use the computer actual Year
        :type for_year: int or None
        :return: Ephem ISO thing localtime
        :rtype: str
        """
        # Try to exit as soon of possible
        if for_year is None:
            if type(self.get_year()) is None:
                for_year = int(datetime.datetime.now().strftime("%Y"))
            else:
                for_year = self.get_year()
        if type(for_year) != int:
            raise TypeError("'for_year' must be a int or None type")

        if for_year is not None and type(for_year) == int:
            return str(ephem.localtime(ephem.next_equinox(str(for_year))).ctime())
        else:
            return str(
                ephem.localtime(ephem.next_equinox(str(self.get_year()))).ctime()
            )

    def get_march_equinox_lasted_days(self, for_year=None):
        d1 = ephem.now()

        if for_year is not None and type(for_year) == int:
            d2 = ephem.next_equinox(str(for_year))
        else:
            d2 = ephem.next_equinox(str(self.get_year()))

        t = d2 - d1
        return "%.0f" % t

    def get_summer_solstice(self, for_year=None):
        """
        Get the June Solstice.

        The day of the summer solstice is the day when the Sun culminates with its higher elevation in the Northern
        hemisphere!

        :param for_year: The year from when get the june solstice, if None it will use the computer actual Year
        :type for_year: int or None
        :return: Ephem ISO thing localtime
        :rtype: str
        """
        # Try to exit as soon of possible
        if for_year is None:
            if type(self.get_year()) is None:
                for_year = int(datetime.datetime.now().strftime("%Y"))
            else:
                for_year = self.get_year()
        if type(for_year) != int:
            raise TypeError("'for_year' must be a int or None type")

        if for_year is not None and type(for_year) == int:
            return str(
                ephem.localtime(ephem.next_summer_solstice(str(for_year))).ctime()
            )
        else:
            return str(
                ephem.localtime(
                    ephem.next_summer_solstice(str(self.get_year()))
                ).ctime()
            )

    def get_summer_solstice_lasted_days(self, for_year=None):
        d1 = ephem.now()
        if for_year is not None:
            d2 = ephem.next_summer_solstice(str(for_year))
        else:
            d2 = ephem.next_summer_solstice(str(self.get_year()))

        t = d2 - d1
        return "%.0f" % t

    def get_september_equinox(self, for_year=None):
        """
        Get the September or Autumn Equinox.

        There are two equinoxes every year – in March and September – when the Sun shines directly on the equator and
        the length of night and day are nearly equal.

        :param for_year: The year from when get the september equinox, if None it will use the computer actual Year
        :type for_year: int or None
        :return: Ephem ISO thing localtime
        :rtype: str
        """
        # Try to exit as soon of possible
        if for_year is None:
            if type(self.get_year()) is None:
                for_year = int(datetime.datetime.now().strftime("%Y"))
            else:
                for_year = self.get_year()
        if type(for_year) != int:
            raise TypeError("'for_year' must be a int or None type")

        if for_year is not None and type(for_year) == int:
            return str(
                ephem.localtime(ephem.next_autumn_equinox(str(for_year))).ctime()
            )
        else:
            return str(
                ephem.localtime(ephem.next_autumn_equinox(str(self.get_year()))).ctime()
            )

    def get_september_equinox_lasted_days(self, for_year=None):
        """

        :param for_year: the year
        :type for_year: int
        :return: a string
        :rtype: str
        """
        d1 = ephem.now()
        if for_year is not None and type(for_year) == int:
            d2 = ephem.next_autumn_equinox(str(for_year))
        else:
            d2 = ephem.next_autumn_equinox(str(self.get_year()))

        t = d2 - d1
        return "%.0f" % t

    def get_winter_solstice(self, for_year=None):
        """
        Get the December or Winter Solstice (or Hibernal solstice).

        The day with the shortest period of daylight and the longest night of the year. In the Northern Hemisphere this
        is the December solstice and in the Southern Hemisphere this is the June solstice.

        :param for_year: The year from when get the december solstice, if None it will use the computer actual Year
        :type for_year: int or None
        :return: Ephem ISO thing localtime
        :rtype: str
        """
        # Try to exit as soon of possible
        if for_year is None:
            if type(self.get_year()) is None:
                for_year = int(datetime.datetime.now().strftime("%Y"))
            else:
                for_year = self.get_year()
        if type(for_year) != int:
            raise TypeError("'for_year' must be a int or None type")

        if for_year is not None and type(for_year) == int:
            return str(
                ephem.localtime(ephem.next_winter_solstice(str(for_year))).ctime()
            )
        else:
            return str(
                ephem.localtime(
                    ephem.next_winter_solstice(str(self.get_year()))
                ).ctime()
            )

    def get_winter_solstice_lasted_days(self, for_year=None):
        """

        :param for_year: the year
        :type for_year: int
        :return: a string
        :rtype: str
        """
        d1 = ephem.now()
        if for_year is not None and type(for_year) == int:
            d2 = ephem.next_winter_solstice(str(for_year))
        else:
            d2 = ephem.next_winter_solstice(str(self.get_year()))

        t = d2 - d1
        return "%.0f" % t

 
    def display_in_number_of_days(self, day=0):
        """

        :param day: a Int it represent the number of day
        :type day: int
        """
        if int(day) <= -2:
            sys.stdout.write("in")
            sys.stdout.write(" ")
            sys.stdout.write(str(day))
            sys.stdout.write(" ")
            sys.stdout.write("days")
        elif int(day) == -1:
            sys.stdout.write("in")
            sys.stdout.write(" ")
            sys.stdout.write(str(day))
            sys.stdout.write(" ")
            sys.stdout.write("day")
        elif int(day) == 0:
            sys.stdout.write("in")
            sys.stdout.write(" ")
            sys.stdout.write(str(day))
            sys.stdout.write(" ")
            sys.stdout.write("day")
        elif int(day) == 1:
            sys.stdout.write("in")
            sys.stdout.write(" ")
            sys.stdout.write(str(day))
            sys.stdout.write("day")
        elif int(day) >= 2:
            sys.stdout.write("in")
            sys.stdout.write(" ")
            sys.stdout.write(str(day))
            sys.stdout.write(" ")
            sys.stdout.write("days")
    

