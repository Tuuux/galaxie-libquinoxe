#! /usr/bin/env python

import sys


class FibonacciNumber(object):
    def __init__(self):
        self.__list_size = None
        self.__list = None

        self.list_size = 0
        self.list = [0, 1]
        self.print = True

    @property
    def list_size(self):
        return self.__list_size

    @list_size.setter
    def list_size(self, value=None):
        if value is None:
            value = 0
        if type(value) != int:
            raise TypeError('"list_size" value must be a int type or None')
        if self.list_size != value:
            self.__list_size = value

    @property
    def list(self):
        return self.__list

    @list.setter
    def list(self, value=None):
        if value is None:
            value = [0, 1]
        if type(value) != list:
            raise TypeError('"list" value must be a list type or None')
        if self.list != value:
            self.__list = value

    def compute(self):
        self.list = None
        while len(self.list) < self.list_size:
            percent = round((len(self.list) / self.list_size) * 100, 3)

            self.list.append(self.list[-1] + self.list[-2])
            if self.print:
                sys.stdout.write("\b" * len("{0}%".format(percent)))
                sys.stdout.write("{0}%\n".format(percent))
                sys.stdout.flush()

        if self.print:
            sys.stdout.write("last number: {0}\n".format(self.list[-1]))
            sys.stdout.flush()


if __name__ == "__main__":
    fibonacci_number = FibonacciNumber()
    fibonacci_number.list_size = 400000
    fibonacci_number.compute()
