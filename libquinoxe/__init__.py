#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: "Tuux" <tuxa@rtnp.org> all rights reserved

from libquinoxe.libs.equinox_dates import EquinoxDates
from libquinoxe.libs.fibonacci_number import FibonacciNumber
from libquinoxe.libs.angle_div_by_power_of_two import Circle

__version__ = "0.3"
