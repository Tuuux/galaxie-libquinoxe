import sys
from libquinoxe.libs.equinox_dates import EquinoxDates

def main():
    Sun = EquinoxDates()
    for year in range(Sun.year, Sun.year + 4):
        sys.stdout.write(f"Year: {year}\n")

        sys.stdout.write(str(f"March equinox:     {Sun.get_march_equinox(for_year=year)} "))
        # sys.stdout.write(Sun.get_march_equinox(for_year=year))
        # sys.stdout.write(str(" "))
        Sun.display_in_number_of_days(
            day=Sun.get_march_equinox_lasted_days(for_year=year)
        )
        
        sys.stdout.write(str("\n"))
        sys.stdout.write(str("Summer Solstice:   "))
        sys.stdout.write(Sun.get_summer_solstice(for_year=year))
        sys.stdout.write(str(" "))
        Sun.display_in_number_of_days(
            day=int(Sun.get_summer_solstice_lasted_days(for_year=year))
        )
        
        sys.stdout.write(str("\n"))
        sys.stdout.write(str("September equinox: "))
        sys.stdout.write(Sun.get_september_equinox(for_year=year))
        sys.stdout.write(str(" "))
        Sun.display_in_number_of_days(
            day=int(Sun.get_september_equinox_lasted_days(for_year=year))
        )
        
        sys.stdout.write(str("\n"))
        sys.stdout.write(str("Winter Solstice:   "))
        sys.stdout.write(Sun.get_winter_solstice(for_year=year))
        sys.stdout.write(str(" "))
        Sun.display_in_number_of_days(
            day=int(Sun.get_winter_solstice_lasted_days(for_year=year))
        )
        sys.stdout.write(str("\n"))
        sys.stdout.write(str("\n"))
        sys.stdout.flush()


